<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->string('type')->default('infinite');
            $table->string('frequency')->default('Month');
            $table->string('frequency_interval')->default('1');
            $table->string('cycles')->default('0');
            $table->float('amount');
            $table->string('currency')->default('USD');
            $table->string('auto_bill_amount')->default('yes');
            $table->string('initial_fail_amount_action')->default('CONTINUE');
            $table->string('paypal_plan_id')->nullable();
            $table->string('stripe_prod_id')->nullable();
            $table->string('stripe_plan_id')->nullable();
            $table->boolean('is_free')->default(false)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
