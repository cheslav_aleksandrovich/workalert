<?php

use App\Plan;
use Stripe\Stripe;
use Stripe\Product;
use Stripe\Plan as StripePlan;
use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Plan::truncate();

        Stripe::setApiKey(env('STRIPE_SECRET'));

        $stripe_product = Product::create([
            "name" => 'Unlimited',
            "description" => 'Unlimited Plan',
        ]);

        $stripe_plan = StripePlan::create(array(
            "amount" => 0,
            "interval" => "month",
            "product" => $stripe_product['id'],
            "currency" => 'usd',
            "interval_count" => 1,
        ));

        Plan::create([
            'name' => 'Unlimited',
            'description' => 'Unlimited Plan',
            'type' => 'infinite',
            'frequency' => 'Month',
            'frequency_interval' => 1,
            'cycles' => '0',
            'amount' => 0,
            'currency' => 'USD',
            'auto_bill_amount' => 'yes',
            'initial_fail_amount_action' => 'CONTINUE',
            'stripe_prod_id' => $stripe_product['id'],
            'stripe_plan_id' => $stripe_plan['id'],
            'is_free' => 1,
            'is_default' => 1,
            'keyword_limit' => 100,
        ]);
    }
}
