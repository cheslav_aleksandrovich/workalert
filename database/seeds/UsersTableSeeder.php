<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $faker = \Faker\Factory::create();
        $password = Hash::make('toptal');

        User::create([
            'name' => 'Administrator',
            'email' => 'admin@test.com',
            'password' => $password,
            'type' => 2,
        ]);

        // And now let's generate a few dozen users for our app:
        User::create([
            'name' => $faker->name,
            'email' => $faker->email,
            'password' => $password,
            'type' => 1,
        ]);
        for ($i = 0; $i < 30; $i++) {
            User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'password' => $password,
                'type' => 0,
            ]);
        }
    }
}
