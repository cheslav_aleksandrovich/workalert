<?php

use Illuminate\Database\Seeder;
use App\Keyword;

class KeywordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Keyword::truncate();

        $faker = \Faker\Factory::create();

        // And now, let's create a few keywords in our database:
        for ($i = 0; $i < 50; $i++) {
            Keyword::create([
                'user_id' => 3,
                'keyword' => $faker->name,
            ]);
        }
    }
}
