<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/pricing', 'PricingController@index')->name('pricing');

// Route::get('create_paypal_plan', 'PaypalController@create_plan');
// Route::get('delete_paypal_plan', 'PaypalController@delete_plan');

Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/subscribe/{id}/paypal', 'SubscriptionController@paypalRedirect')
        ->name('paypal.redirect');
    Route::get('/subscribe/{id}/paypal/return', 'SubscriptionController@paypalReturn')
        ->name('paypal.return');

    Route::resource('subscriptions', 'SubscriptionController');

    Route::group(['middleware' => ['admin'], 'prefix' => 'admin'], function() {
        Route::group(['middleware' => ['superAdmin'], 'prefix' => 'billing'], function() {
            Route::resource('plans', 'Admin\AdminPlanController');
        });
    });
});
