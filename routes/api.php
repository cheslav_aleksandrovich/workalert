<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', 'Auth\ApiAuthController@user');

Route::group(['middleware' => ['cors', 'json.response']], function () {

    Route::post('/login', 'Auth\ApiAuthController@login')->name('login.api');
    Route::post('/register', 'Auth\ApiAuthController@register')->name('register.api');
    Route::post('/forgot-password', 'Auth\ApiAuthController@forgotPassword');

    Route::middleware('auth:api')->group(function () {
        // Route::get('/keywords', 'KeywordController@index')->middleware('api.admin')->name('keywords');
        Route::get('/users', 'Auth\ApiAuthController@getAuthUser');

        Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');
        Route::resource('keywords', 'Api\KeywordAPIController', ['only' => ['index', 'show', 'destroy', 'store']]);
        Route::resource('plans', 'Api\PlanAPIController', ['only' => ['index']]);
    });

    Route::group(['prefix' => 'mobile', 'middleware' => ['mobile']], function () {
        Route::post('login', 'Mobile\AuthController@login');
        Route::post('register', 'Mobile\AuthController@register');

        Route::middleware('auth:api')->group(function () {
            Route::get('user', 'Mobile\AuthController@getAuthUser');
            Route::post('logout', 'Mobile\AuthController@logout')->name('logout.api');
            Route::resource('keywords', 'Mobile\KeywordController', ['only' => ['index']]);
        });
    });
});
