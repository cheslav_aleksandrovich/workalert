<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});



Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    //   $exitCode = Artisan::call('optimize');
    //     $exitCode = Artisan::call('route:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('view:cache');
    $exitCode = Artisan::call('config:cache');
    // return what you want
});

Auth::routes();
Route::get('/', 'HomeController@baseUrl')->middleware('referral')->name('');
Route::get('/pricing', 'PricingController@index')->name('pricing');

// Route::get('create_paypal_plan', 'PaypalController@create_plan');

Route::get('/subscribe/paypal', 'PaypalController@paypalRedirect')->name('paypal.redirect');
Route::get('/subscribe/paypal/return', 'PaypalController@paypalReturn')->name('paypal.return');
// Route::get('delete_paypal_plan', 'PaypalController@delete_plan');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'PricingController@index')->name('pricing');
    //  Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('keywords', 'KeywordController', ['only' => ['destroy', 'store']]);

    /** PayPal Subscription */
    Route::get('/subscribe/{id}/paypal', 'SubscriptionController@paypalRedirect')
        ->name('paypal.redirect');
    Route::get('/subscribe/{id}/paypal/return', 'SubscriptionController@paypalReturn')
        ->name('paypal.return');
    Route::get('/subscribe/{id}/paypal/cancel', 'SubscriptionController@paypalCancel')
        ->name('paypal.cancel');

    /** Stripe Subscription */
    Route::get('/subscribe/{id}/stripe', 'SubscriptionController@stripeRedirect')
        ->name('stripe.redirect');
    Route::post('/subscribe/stripe', 'SubscriptionController@stripeSubscribe')
        ->name('stripe.subscribe');
    Route::post('/subscribe/stripe/trial', 'SubscriptionController@stripeTrial')
        ->name('stripe.trial');
    Route::get('/subscribe/{id}/stripe/cancel', 'SubscriptionController@stripeCancel')
        ->name('stripe.cancel');

    Route::resource('subscriptions', 'SubscriptionController');

    Route::group(['middleware' => ['admin'], 'prefix' => 'admin'], function () {
        Route::get('/', 'Admin\HomeController@index')->name('admin.index');
        Route::get('/download-users-email', 'Admin\HomeController@genearateUserEmailCSV')->name('admin.index.download');
        Route::group(['middleware' => ['superAdmin'], 'prefix' => 'billing'], function () {
            Route::resource('plans', 'Admin\AdminPlanController');
        });
    });
});
