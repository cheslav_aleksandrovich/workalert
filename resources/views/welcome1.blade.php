<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            body {
    background-color: #000000;
    font-family: Roboto,sans-serif;
    font-weight: 400;
    margin-bottom: 150px;
    padding-top: 0;
}



.logo{padding-top:10px;color:#09ef6b;}

.hover{text-decoration:none!important;}

.main-text{color:white;font-size:12px;padding-top:5px;padding-bottom:5px;}

.custom-text{color:#119747;font-size:12px;padding-top:260px;}

.panel {
    background-color: #1a1a1a!important;border-radius:0px!important;
    }

.panel-body {padding-top:50px;}

.fields{padding-bottom:50px;}

.plan{border:1px solid #111111;margin-left:5px;margin-right:5px;border-radius: 4px;padding-bottom: 10px;margin-top:10px;}

.plan:hover{border:1px solid #1CB94E;}

.plan-heading{color:#FFFFFF;font-size:12px;padding-top:20px;}
.plan-price{color:#FFFFFF;font-size:15px;padding-top:5px;}
.plan-keyword {color:#FFFFFF;font-size:12px;margin-top:5px;}
.panel-login {
	-webkit-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	-moz-box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
	box-shadow: 0px 2px 3px 0px rgba(0,0,0,0.2);
}
.panel-login>.panel-heading {
	color: #00415d;
	background-color: #1a1a1a;
	text-align:center;
}
.panel-login>.panel-heading a{
	text-decoration: none;
	color: #666;
	font-weight: bold;
	font-size: 12px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login>.panel-heading a.active{
	padding-left: 40px;
	padding-right: 40px;
	padding-bottom: 21px;
	border-bottom: 2px solid #109847;
	color: #109847;
	font-size: 15px;
}

.panel-login input[type="text"],.panel-login input[type="email"],.panel-login input[type="password"] {
	background-color: #0d0d0d;
	height: 45px;
	border: 1px solid #ddd;
	font-size: 12px;
	-webkit-transition: all 0.1s linear;
	-moz-transition: all 0.1s linear;
	transition: all 0.1s linear;
}
.panel-login input:hover,
.panel-login input:focus {
	outline:none;
	-webkit-box-shadow: none;
	-moz-box-shadow: none;
	box-shadow: none;
	border-color: #ccc;
}
.btn-login {
	background-color: #09ef6b;
	outline: none;
	color: #fff;
	font-size: 12px;
	height: auto;
	font-weight: normal;
	text-transform: uppercase;
	
}

.btn-login:hover,
.btn-login:focus {
	color: #fff;
	background-color: #000000;
	
}
.forgot-password {
	font-size:12px;
	color:#119747;
}
.forgot-password:hover,
.forgot-password:focus {
	
	color: #666;
}

.btn-register {
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 12px;
	height: auto;
	font-weight: normal;
	padding: 14px 0;
	text-transform: uppercase;
	border-color: #1CB94A;
}
.btn-register:hover,
.btn-register:focus {
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}

.btn-upgrade {
	width:80%;
	margin-left:5px;
	margin-right:5px;
	margin-top:5px;
	background-color: #1CB94E;
	outline: none;
	color: #fff;
	font-size: 12px;
	height: auto;
	font-weight: normal;
	text-transform: uppercase;
	border-color: #1CB94A;
}

.btn-upgrade:hover,
.btn-upgrade:focus {
	width:80%;
	margin-left:5px;
	margin-right:5px;
	color: #fff;
	background-color: #1CA347;
	border-color: #1CA347;
}
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-left links">
                <a href="{{ route('pricing') }}">{{ __('Pricing') }}</a>
            </div>
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Welcome to Upwork Job Tracker
                </div>
            </div>
        </div>
    </body>
</html>
