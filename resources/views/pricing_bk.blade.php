@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach ($plans as $plan)
        <div class="col-sm-4 col-xs-12">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">{{ $plan->name }}</div>
                </div>

                <div class="card-body">
                    <div class="pull-left">{{ $plan->description }}</div>
                    <div class="pull-left">{{ $plan->keyword_limit }} {{ __('Keyword(s)')}}</div>
                    <div>{{ $plan->amount }}{{ $plan->currency }} per {{ $plan->frequency_interval }}{{ $plan->frequency }}</div>
                </div>
                <div class="card-footer">
                    <div class="col-xs-6">
                        @guest
                        <a class="btn btn-primary" href="{{ route('paypal.redirect', $plan->paypal_plan_id) }}">{{ __('Upgrade Now') }}</a><br />
                        @else
                            @if (Auth::user() and Auth::user()->subscription() and Auth::user()->subscription()->plan_id == $plan->id)
                            <a class="btn btn-success" href="#">{{ __('Current Plan') }}</a><br />
                            @else
                            <a class="btn btn-primary" href="{{ route('paypal.redirect', $plan->paypal_plan_id) }}">{{ __('Update Now') }}</a><br />
                            @endif
                        @endguest
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
