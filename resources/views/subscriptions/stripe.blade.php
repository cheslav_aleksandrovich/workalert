@extends('layouts.login')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="panel panel-login">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-12">
                            <a href="#" class="active hover" id="login-form-link">Upgrade</a>
                        </div>
                    </div>
                    <hr>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card bg-transparent">
                                <input class="form-control" id="card-holder-name" type="text"
                                    placeholder="Card Holder Name">
                                <!-- Stripe Elements Placeholder -->
                                <div class="form-control my-5" id="card-element"></div>
                                @if ($localPlan->is_free == 0 && !auth()->user()->referred_by)
                                <input class="form-control" name="referr_code" id="referr" type="text"
                                    placeholder="Referr Code">
                                @endif
                                <div class="form-group mt-5">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <button class="form-control btn btn-login" id="card-button"
                                                data-secret="{{ $intent->client_secret }}"
                                                style="height: 60px">{{ $try_out_trial ? 'Try out trial' : 'Subscribe' }}</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mt-5">
                                    <div class="row">
                                        <div class="col-sm-4 col-sm-offset-4">
                                            <img src="{{ asset('assets/images/logo-stripe-secure-payments.png') }}"
                                                alt="" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-12">
            <div class="card">
                <input id="card-holder-name" type="text" placeholder="Card Holder Name">

                <!-- Stripe Elements Placeholder -->
                <div id="card-element"></div>

                <button id="card-button" data-secret="{{ $intent->client_secret }}">Subscribe</button>
    </div>
</div> --}}
</div>
</div>
@endsection

@section('styles')
<style>
    .col-xs-6 {
        width: 50%;
    }

    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) {
        .col-xs-6 {
            width: 49% !important;
        }
    }
</style>
@endsection

@section('scripts')
<script src="https://js.stripe.com/v3/"></script>

<script>
    const stripe = Stripe('{{env('STRIPE_KEY')}}');

    const elements = stripe.elements();
    const cardElement = elements.create('card', {
        style: {
            base: {
                iconColor: '#000000',
                color: '#000000',
                fontWeight: 500,
                fontFamily: 'Roboto, Open Sans, Segoe UI, sans-serif',
                fontSize: '16px',
                fontSmoothing: 'antialiased',
                backgroundColor: '#ffffff',
                margin: '10px 0',
                ':-webkit-autofill': {
                    color: '#fce883',
                },
                '::placeholder': {
                    color: '#000000',
                },
            },
            invalid: {
                iconColor: '#FFC7EE',
                color: '#FFC7EE',
            },
        }, 
    });

    cardElement.mount('#card-element');

    const cardHolderName = document.getElementById('card-holder-name');
    const cardButton = document.getElementById('card-button');
    const clientSecret = cardButton.dataset.secret;

    cardButton.addEventListener('click', async (e) => {
        const { setupIntent, error } = await stripe.confirmCardSetup(
            clientSecret, {
                payment_method: {
                    card: cardElement,
                    billing_details: { name: cardHolderName.value }
                }
            }
        );

        if (error) {
            alert(error)
            console.log('error :>> ', error);
        } else {
            var url = "{{ $try_out_trial ? "/subscribe/stripe/trial" : "/subscribe/stripe"}}"
            document.getElementById("card-button").disabled = true;
            axios.post(url, {
                planID: '{{ $planId }}',
                paymentMethod: setupIntent.payment_method,
                @if ($localPlan->is_free == 0 && !auth()->user()->referred_by)
                referrCode: document.getElementById('referr').value
                @endif
            })
            .then((response) => {
                window.location.href="https://mind2matter.co/thank-you-7";
            })
            .catch(error => {
                const newError = Object.assign({}, error);
                alert(newError.response.data.errors)
                console.log('error :>> ', newError.response.data.errors);
                // location.reload();
            });
            
        }

    });
</script>
@endsection