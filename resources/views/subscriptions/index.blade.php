@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">{{ __('Plans') }}</div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered">
                        <tr>
                            <th>{{ __('No') }}</th>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Price') }}</th>
                            <th>{{ __('Expred At') }}</th>
                        </tr>
                        @foreach ($subscriptions as $index => $subscription)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $subscription->plan->name }}</td>
                            <td>{{ $subscription->amount }}</td>
                            <td>{{ $subscription->expired_at }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <div class="card-footer">
                    <div class="col-xs-6">
                        <a class="btn btn-primary" href="">What?</a><br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
