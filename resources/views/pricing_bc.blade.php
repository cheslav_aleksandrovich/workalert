@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">

        <div class="text-center main-text">
            <p style="margin-top: 10px;"><b>Add your keywords</b></p>
        </div>

        <div class="panel panel-login">
            <div class="row m-0">
                <div class="col-xs-10 col-xs-offset-1 py-5">

                    @foreach (auth()->user()->keywords() as $keyword)
                    <div class="row each-keyword">
                        <div class="col-xs-3">
                            <h1 class="m-0" style="line-height: 46px;">{{ $keyword->keyword }}</h1>
                        </div>
                        <div class="col-xs-7 py-1">
                            <h3 class="m-0" style="line-height: 46px;overflow: hidden;">{{ $keyword->rss }}</h3>
                        </div>
                        <div class="col-xs-2">
                            <form action="{{ route('keywords.destroy', $keyword->id) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button type="submit"
                                    style="width: 46px;height: 46px;line-height: 46px;padding: 0;font-size: 14px;background: #a22e2e; color: #fff"
                                    class="btn btn-default website-form-button">Del</button>
                            </form>
                        </div>
                    </div>
                    @endforeach

                    <form method="POST" action="{{ route('keywords.store') }}">
                        @csrf
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="form-group mb-0">
                                    <input type="text"
                                        class="form-control website-form-control @error('keyword') is-invalid @enderror"
                                        id="keyword" placeholder="Keyword" name="keyword">
                                    @error('keyword')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xs-7">
                                <div class="form-group mb-0">
                                    <input type="text"
                                        class="form-control website-form-control @error('rss') is-invalid @enderror"
                                        id="rss" placeholder="UpWork RSS Link" name="rss">
                                    @error('rss')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <button type="submit" style="width: 46px;height: 46px;line-height: 46px;padding: 0;"
                                    class="btn btn-default website-form-button">+</button>
                            </div>
                        </div>
                    </form>
                    @if (session('success'))
                    <div class="alert alert-success mt-4" role="alert">
                        {{ session('success') }}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-8 col-md-offset-2">

        <div class="text-center main-text">
            <p style="margin-top: 10px;"><b>Select Your Plan</b></p>
        </div>

        <div class="panel panel-login">

            <div class="row">
                @foreach ($plans as $plan)
                <div class="col-xs-4">
                    <div class="plan" id="basic">
                        <div class="plan-heading">
                            <div class="text-center">
                                {{ $plan->name }}
                            </div>
                        </div>

                        <!--<div class="pull-left">{{ $plan->description }}</div>-->


                        <div class="plan-price">
                            <div class="text-center">
                                @if ($plan->is_free)
                                <b>
                                    Free
                                </b>
                                @else
                                <b>
                                    {{ $plan->currency }} {{ $plan->amount }} / {{ $plan->frequency }}
                                </b>
                                @endif
                            </div>
                        </div>

                        <div class="plan-keyword">
                            <div class="text-center" style="color: grey;">
                                {{ $plan->keyword_limit }} {{ __('Keyword(s)')}}
                            </div>
                        </div>
                        <div class="text-center">
                            @guest
                            {{-- 
                                <a class="form-control btn btn-upgrade" href="{{ route('paypal.redirect', $plan->paypal_plan_id) }}">{{ __('Upgrade Now') }}</a><br />
                            --}}
                            <a class="form-control btn btn-upgrade" href="#">{{ __('Upgrade') }}</a><br />
                            <a class="form-control btn btn-upgrade" href="#">{{ __('Upgrade') }}</a><br />
                            @else
                            @if (
                            Auth::user()->subscription() and
                            Auth::user()->subscribed('default', $plan->stripe_plan_id)
                            )
                            <a class="form-control btn btn-upgrade" href="#">{{ __('Current') }}</a><br />
                            <a class="form-control btn btn-upgrade" style="font-size:8px!important;"
                                href="{{route('stripe.cancel', Auth::user()->subscription()->stripe_plan	)}}">{{ __('Cancel') }}</a><br />
                            @else
                            @if ($plan->is_free)
                            <a class="form-control btn btn-upgrade"
                                href="{{ route('stripe.redirect', $plan->stripe_plan_id) }}">{{ __('Select') }}</a>
                            @else
                            <a class="form-control btn btn-upgrade"
                                href="{{ route('stripe.redirect', $plan->stripe_plan_id) }}">{{ __('Upgrade') }}</a><br />
                            {{-- <a class="form-control btn btn-upgrade"
                                href="{{ route('stripe.redirect', ['id' => $plan->stripe_plan_id, 'try_out_trial' => 'true']) }}">{{ __('Try out trial for 14 days') }}</a><br />
                            --}}
                            @endif
                            @endif

                            @endguest
                            <!--<button class="form-control btn btn-upgrade">Upgrade</button>-->
                        </div>
                    </div>
                </div>
                @endforeach
                <!--                <div class="col-xs-4" style="padding-left:0px!important;padding-right:0px!important;">
                                    <div class="plan active" id="standard">
                                        <div class="plan-heading">
                                            <div class="text-center">
                                                Basic
                                            </div>
                                        </div>
                                        <div class="plan-price">
                                            <div class="text-center">
                                                $4.99/Mo
                                            </div>
                                        </div>
                
                                        <div class="plan-keyword">
                                            <div class="text-center">
                                                1 keyword
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button class="form-control btn btn-upgrade">Upgrade</button>
                                        </div>
                                    </div>
                                </div>
                
                                <div class="col-xs-4" style="padding-left:0px!important;padding-right:12px!important;">
                                    <div class="plan" id="premium">
                                        <div class="plan-heading">
                                            <div class="text-center">
                                                Basic
                                            </div>
                                        </div>
                                        <div class="plan-price">
                                            <div class="text-center">
                                                $4.99/Mo
                                            </div>
                                        </div>
                
                                        <div class="plan-keyword">
                                            <div class="text-center">
                                                1 keyword
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <button class="form-control btn btn-upgrade">Upgrade</button>
                                        </div>
                                    </div>
                                </div>-->
            </div>

            <div class="row justify-content-center mt-5">
                <div class="col-md-6 col-lg-4">
                    <div class="plan">
                        <div class=" text-center main-text p-3">
                            <b class="text-white">Referr Someone</b>
                            <input type="text" class="form-control bg-white text-center my-2"
                                value="{{ auth()->user()->affiliate_id }}" readonly>
                            <input type="text" class="form-control bg-white text-center my-2"
                                value="{{ auth()->user()->getReferralLink() }}"
                                style="text-overflow: ellipsis;overflow: hidden;white-space: normal;" readonly>
                            <div class="my-5">
                                <b class="text-white">Total Referral Points:</b>
                                <b class="text-white">{{ auth()->user()->getAmmountOff() }}</b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">

                            <p class="custom-text"> Contact For Custom Quantity</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <p>One fine body&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection