<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8">
    <meta name="viewport">
    <meta name="keywords" />
    <meta name="description" />
    <title>packages</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet"
        id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
        crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body {
            background-color: #000000;
            font-family: Roboto, sans-serif;
            font-weight: 400;
            margin-bottom: 150px;
            padding-top: 0;
        }



        .logo {
            padding-top: 10px;
            color: #09ef6b;
        }

        .hover {
            text-decoration: none !important;
        }

        .main-text {
            color: white;
            font-size: 12px;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .custom-text {
            color: #119747;
            font-size: 12px;
            padding-top: 260px;
        }

        .panel {
            background-color: #1a1a1a !important;
            border-radius: 0px !important;
        }

        .panel-body {
            padding-top: 50px;
        }

        .fields {
            padding-bottom: 50px;
        }

        .plan {
            border: 1px solid #111111;
            border-radius: 4px;
            padding-bottom: 10px;
            margin-top: 10px;
        }

        .plan:hover {
            border: 1px solid #1CB94E;
        }

        .plan-heading {
            color: #FFFFFF;
            font-size: 12px;
            padding-top: 20px;
        }

        .plan-price {
            color: #FFFFFF;
            font-size: 15px;
            padding-top: 5px;
        }

        .plan-keyword {
            color: #FFFFFF;
            font-size: 12px;
            margin-top: 5px;
        }

        .panel-login {
            -webkit-box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.2);
            box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.2);
        }

        .panel-login>.panel-heading {
            color: #00415d;
            background-color: #1a1a1a;
            text-align: center;
        }

        .panel-login>.panel-heading a {
            text-decoration: none;
            color: #666;
            font-weight: bold;
            font-size: 12px;
            -webkit-transition: all 0.1s linear;
            -moz-transition: all 0.1s linear;
            transition: all 0.1s linear;
        }

        .panel-login>.panel-heading a.active {
            padding-left: 40px;
            padding-right: 40px;
            padding-bottom: 21px;
            border-bottom: 2px solid #109847;
            color: #109847;
            font-size: 15px;
        }

        .panel-login input[type="text"],
        .panel-login input[type="email"],
        .panel-login input[type="password"] {
            background-color: #0d0d0d;
            height: 45px;
            border: 1px solid #ddd;
            font-size: 12px;
            -webkit-transition: all 0.1s linear;
            -moz-transition: all 0.1s linear;
            transition: all 0.1s linear;
        }

        .panel-login input:hover,
        .panel-login input:focus {
            outline: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            border-color: #ccc;
        }

        .btn-login {
            background-color: #09ef6b;
            outline: none;
            color: #fff;
            font-size: 12px;
            height: auto;
            font-weight: normal;
            text-transform: uppercase;

        }

        .btn-login:hover,
        .btn-login:focus {
            color: #fff;
            background-color: #000000;

        }

        .forgot-password {
            font-size: 12px;
            color: #119747;
        }

        .forgot-password:hover,
        .forgot-password:focus {

            color: #666;
        }

        .btn-register {
            background-color: #1CB94E;
            outline: none;
            color: #fff;
            font-size: 12px;
            height: auto;
            font-weight: normal;
            padding: 14px 0;
            text-transform: uppercase;
            border-color: #1CB94A;
        }

        .btn-register:hover,
        .btn-register:focus {
            color: #fff;
            background-color: #1CA347;
            border-color: #1CA347;
        }

        .btn-upgrade {
            font-size: 10px !important;
            width: 80%;
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 5px;
            background-color: #1CB94E;
            outline: none;
            color: #fff;
            font-size: 12px;
            height: auto;
            font-weight: normal;
            text-transform: uppercase;
            border-color: #1CB94A;
        }

        .btn-upgrade:hover,
        .btn-upgrade:focus {
            width: 80%;
            margin-left: 5px;
            margin-right: 5px;
            color: #fff;
            background-color: #1CA347;
            border-color: #1CA347;
        }

        .col-xs-4 {
            width: 32.33333333% !important;
            margin-left: 5px;
            margin-right: -5px;
        }

        .col-md-6 {
            width: 50%;
        }

        @media only screen and (min-device-width : 375px) and (max-device-width : 667px) {
            .col-md-6 {
                width: 49%;
            }
        }

        _::-webkit-full-page-media,
        _:future,
        :root .row::before {
            display: none;
        }

        .website-form-control,
        .website-form-control::-webkit-input-placeholder {
            font-size: 14px;
            color: #404040;
        }

        .website-form-control {
            border: 1px solid #333333 !important;
            border-radius: 10px;
            padding: 0 15px;
            background: #0d0d0d;
        }

        .website-form-button {
            background: #333333;
            color: #4D4D4D;
            padding: 15px;
            border-radius: 10px;
            font-size: 20px;
        }

        .each-keyword {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            background: #1a1a1a;
            color: #fff;
            margin-bottom: 15px;
            border-radius: 10px;
        }

        .each-keyword .del-btn {
            /* padding: 20px; */
            -ms-flex: 0 0 10%;
            flex: 0 0 10%;
            max-width: 10%;
            background: #262626;
            color: #4c4c4c;
            border-radius: 10px;
            text-align: center;
            border-top-right-radius: 0%;
            border-bottom-right-radius: 0%;
            font-size: 16px;
            height: 50px;
            line-height: 50px;
        }

        .each-keyword .del-btn button {
            background: none;
            border: none;
        }

        .each-keyword .keyword-name {
            -ms-flex: 0 0 40%;
            flex: 0 0 40%;
            max-width: 40%;
            font-size: 16px;
            padding-left: 25px;
            /* padding-top: 20px; */
            line-height: 50px;
        }

        .each-keyword .keyword-info {
            -ms-flex: 0 0 50%;
            flex: 0 0 50%;
            max-width: 50%;
            font-size: 16px;
            padding-left: 25px;
            /* padding-top: 20px; */
            font-weight: bold;
            line-height: 50px;
        }
    </style>
</head>
<header>
    <div class="container-fluid">
        <div class="row" style="background-color:#1a1a1a;">
            <div class="col-md-6">

                <a href="/home" class="hover">
                    <img src="/assets/images/WorkAlert.png" style="width:100px;margin-top: 8px;">
                    <!--<p class="logo">Work Alert</p>        -->
                </a>

            </div>
            <div class="col-md-6" style="text-align:right;">

                <button class="form-control btn btn-upgrade" style="width:60px!important;margin-top:20px;" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}



                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
            </div>

            </button>
        </div>
    </div>
</header>


<body>




    <div class="container">
        @yield('content')
    </div>



    <!-- Optional JavaScript -->
    <script src="js/jquery-3.3.1.min.js"></script>

</body>

</html>