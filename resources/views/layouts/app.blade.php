<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--   <title>{{ config('app.name', 'Work Alert') }}</title> -->
    <title> Work Alert</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body {
            background-color: #000000;
            font-family: Roboto, sans-serif;
            font-weight: 400;
            margin-bottom: 150px;
            padding-top: 0;
        }



        .logo {
            padding-top: 10px;
            color: #09ef6b;
        }

        .hover {
            text-decoration: none !important;
        }

        .main-text {
            color: white;
            font-size: 12px;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .custom-text {
            color: #119747;
            font-size: 12px;
            padding-top: 260px;
        }

        .panel {
            background-color: #1a1a1a !important;
            border-radius: 0px !important;
        }

        .panel-body {
            padding-top: 50px;
        }

        .fields {
            padding-bottom: 50px;
        }

        .plan {
            border: 1px solid #111111;
            margin-left: 5px;
            margin-right: 5px;
            border-radius: 4px;
            padding-bottom: 10px;
            margin-top: 10px;
        }

        .plan:hover {
            border: 1px solid #1CB94E;
        }

        .plan-heading {
            color: #FFFFFF;
            font-size: 12px;
            padding-top: 20px;
        }

        .plan-price {
            color: #FFFFFF;
            font-size: 15px;
            padding-top: 5px;
        }

        .plan-keyword {
            color: #FFFFFF;
            font-size: 12px;
            margin-top: 5px;
        }

        .panel-login {
            -webkit-box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.2);
            box-shadow: 0px 2px 3px 0px rgba(0, 0, 0, 0.2);
        }

        .panel-login>.panel-heading {
            color: #00415d;
            background-color: #1a1a1a;
            text-align: center;
        }

        .panel-login>.panel-heading a {
            text-decoration: none;
            color: #666;
            font-weight: bold;
            font-size: 12px;
            -webkit-transition: all 0.1s linear;
            -moz-transition: all 0.1s linear;
            transition: all 0.1s linear;
        }

        .panel-login>.panel-heading a.active {
            padding-left: 40px;
            padding-right: 40px;
            padding-bottom: 21px;
            border-bottom: 2px solid #109847;
            color: #109847;
            font-size: 15px;
        }

        .panel-login input[type="text"],
        .panel-login input[type="email"],
        .panel-login input[type="password"] {
            background-color: #0d0d0d;
            height: 45px;
            border: 1px solid #ddd;
            font-size: 12px;
            -webkit-transition: all 0.1s linear;
            -moz-transition: all 0.1s linear;
            transition: all 0.1s linear;
        }

        .panel-login input:hover,
        .panel-login input:focus {
            outline: none;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            border-color: #ccc;
        }

        .btn-login {
            background-color: #09ef6b;
            outline: none;
            color: #fff;
            font-size: 12px;
            height: auto;
            font-weight: normal;
            text-transform: uppercase;

        }

        .btn-login:hover,
        .btn-login:focus {
            color: #fff;
            background-color: #000000;

        }

        .forgot-password {
            font-size: 12px;
            color: #119747;
        }

        .forgot-password:hover,
        .forgot-password:focus {

            color: #666;
        }

        .btn-register {
            background-color: #1CB94E;
            outline: none;
            color: #fff;
            font-size: 12px;
            height: auto;
            font-weight: normal;
            padding: 14px 0;
            text-transform: uppercase;
            border-color: #1CB94A;
        }

        .btn-register:hover,
        .btn-register:focus {
            color: #fff;
            background-color: #1CA347;
            border-color: #1CA347;
        }

        .btn-upgrade {
            font-size: 10px !important;
            width: 90%;
            margin-left: 5px;
            margin-right: 5px;
            margin-top: 5px;
            background-color: #1CB94E;
            outline: none;
            color: #fff;
            font-size: 12px;
            height: auto;
            font-weight: normal;
            text-transform: uppercase;
            border-color: #1CB94A;
        }

        .btn-upgrade:hover,
        .btn-upgrade:focus {
            width: 80%;
            margin-left: 5px;
            margin-right: 5px;
            color: #fff;
            background-color: #1CA347;
            border-color: #1CA347;
        }
    </style>
    @yield('styles')
</head>

<body>
    <div class="container-fluid">
        <div class="row" style="background-color:#1a1a1a;">

            <div class="col-md-6">

                <a href="/home" class="hover">
                    <img src="/assets/images/WorkAlert.png" style="width:100px;">
                    <!--<p class="logo">Work Alert</p>        -->
                </a>

            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">
                    <li><a href="{{ route('pricing') }}">{{ __('Pricing') }}</a></li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                    </li>
                    @endif
                    @else

                    <div class="col-md-6" style="text-align:right;">

                        <button class="form-control btn btn-upgrade"
                            style="width:60px!important;margin-top:25px;margin-bottom:10px;" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}



                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                    </div>
                    Logout
                    </button>
            </div>
        </div>
    </div>
    @endguest
    </ul>
    </div>
    </div>
    </nav>

    <main class="py-4">
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        @yield('content')
    </main>
    </div>
    @yield('scripts')
</body>

</html>