@extends('layouts.login')

<style>
    
     .col-xs-6{width:50%;}
    
       @media only screen 
and (min-device-width : 375px) 
and (max-device-width : 667px) {.col-xs-6{width:49%!important;}}
</style>

@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-login">
            <div class="panel-heading">
                <div class="row">

                    <div class="col-xs-6">
                        <a href="{{ route('login') }}" class="active hover" id="login-form-link">Login</a>
                    </div>
                    <div class="col-xs-6">
                        <a href="{{ route('register') }}" id="register-form-link">Sign Up</a>
                    </div>
                </div>
                <hr>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="login-form" action="{{ route('login') }}" method="post" role="form" style="display: block;">
                            @csrf

                            <div class="fields">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" tabindex="1" class="form-control @error('email') is-invalid @enderror" placeholder="Email Address" value="{{ old('email') }}">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" id="password" tabindex="2" class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            @if (Route::has('password.request'))
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="text-center" >
                                            <a href="{{ route('password.request') }}" tabindex="5" class="forgot-password">Forgot Password?</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif

                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="LogIn">
                                    </div>
                                </div>
                            </div>

                        </form>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
