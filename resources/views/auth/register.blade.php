@extends('layouts.login')

<style>
    .col-xs-6 {
        width: 50%;
    }

    @media only screen and (min-device-width : 375px) and (max-device-width : 667px) {
        .col-xs-6 {
            width: 49% !important;
        }
    }
</style>

@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-login">
            <div class="panel-heading">
                <div class="row">

                    <div class="col-xs-6" style="width:50%;">
                        <a href="{{ route('login') }}" id="login-form-link">Login</a>
                    </div>
                    <div class="col-xs-6" style="width:50%;">
                        <a href="{{ route('register') }}" class="active hover" id="register-form-link">Sign Up</a>
                    </div>
                </div>
                <hr>
            </div>

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form id="register-form" action="{{ route('register') }}" method="post" role="form">
                            @csrf
                            <div class="fields">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" tabindex="1"
                                        class="form-control @error('email') is-invalid @enderror"
                                        placeholder="Email Address" value="{{ old('email') }}">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" id="password" tabindex="2"
                                        class="form-control @error('password') is-invalid @enderror"
                                        placeholder="Password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" id="confirm-password"
                                        tabindex="2" class="form-control" placeholder="Confirm Password">
                                </div>
                                <div class="form-group">
                                    <input type="text" name="referral" id="referral" tabindex="2"
                                        class="form-control @error('referral') is-invalid @enderror"
                                        placeholder="Referral Code" @if (Cookie::get('referral'))
                                        value="{{ Cookie::get('referral') }}" @endif>
                                    @error('referral')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <input type="submit" name="register-submit" id="register-submit" tabindex="4"
                                            class="form-control btn btn-register" value="Sign Up">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection