@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2 pt-5">
        <div style="color: #fff; font-size: 14px; margin-bottom: 50px;">
            1. Setup your Keywords by doing an Upwork Search
            and taking the RSS link. <br>
            2. To get more keywords, refer your friends to the app. <br>
            3. Get the job Notifications for your keywords from our mobile app <br>
        </div>
        @foreach (auth()->user()->all_keywords() as $keyword)
        <div class="each-keyword">
            <div class="del-btn">
                <form action="{{ route('keywords.destroy', $keyword->id) }}" method="POST">
                    @method('DELETE')
                    @csrf
                    <button type="submit" style="">
                        <i class="far fa-trash-alt"></i>
                    </button>
                </form>
            </div>
            <div class="keyword-name">
                {{ $keyword->keyword }}
            </div>
            <div class="keyword-info">
                {{ Str::limit($keyword->rss, 25) }}
            </div>
        </div>
        @endforeach

        @if (auth()->user()->all_keywords()->count() != auth()->user()->getAmmountOfKeywords() and auth()->user()->all_keywords()->count() < auth()->user()->getAmmountOfKeywords())
            <div>
                <form method="POST" action="{{ route('keywords.store') }}">
                    @csrf
                    <div class="row">
                        <div class="col-xs-3">
                            <div class="form-group mb-0">
                                <input type="text"
                                    class="form-control website-form-control @error('keyword') is-invalid @enderror"
                                    id="keyword" placeholder="Keyword" name="keyword" style="height: 46px;">
                                @error('keyword')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xs-7">
                            <div class="form-group mb-0">
                                <input type="text"
                                    class="form-control website-form-control @error('rss') is-invalid @enderror"
                                    id="rss" placeholder="UpWork RSS Link" name="rss" style="height: 46px;">
                                @error('rss')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-xs-2">
                            <button type="submit" style="width: 46px;height: 46px;line-height: 46px;padding: 0;"
                                class="btn btn-default website-form-button">+</button>
                        </div>
                    </div>
                </form>
                @if (session('success'))
                <div class="alert alert-success mt-4" role="alert">
                    {{ session('success') }}
                </div>
                @endif
            </div>
            @endif

            <div style="color: #09ee6a; font-size: 14px; margin-top: 40px; text-align: center;">
                Download Our App <br>
                After setting your keywords <br>
                <a href="#">
                    <img class="img-fluid mt-4 mr-4" src="{{ asset('assets/images/google_play.png') }}" alt="">
                </a>
                <a href="#">
                    <img class="img-fluid mt-4" src="{{ asset('assets/images/apple_store.png') }}" alt="">
                </a>
                <iframe class="mt-5" width="560" height="315" src="https://www.youtube.com/embed/6HosPjdOwkI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
    </div>
</div>
@endsection