@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Edit Plan') }}{{ $plan->paypal_plan_id }}</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    @if ($errors = Session::get('errors'))
                    @foreach ($errors->all() as $message)
                    <div class="alert alert-danger">
                        <span>{{ $message }}</span>
                    </div>
                    @endforeach
                    @endif

                    <form action="{{ route('plans.update', $plan->id) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>{{ __('Name') }}:</strong>
                                    <input type="text" name="name" value="{{ $plan->name }}" class="form-control"
                                        placeholder="{{ __('Name') }}">
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>{{ __('Description') }}:</strong>
                                    <textarea class="form-control" style="height:150px" name="description"
                                        placeholder="{{ __('Description') }}">{{ $plan->description }}</textarea>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>{{ __('Frequency Interval Month') }}:</strong>
                                    <input class="form-control" type="number" name="frequency_interval"
                                        value="{{ $plan->frequency_interval }}" min="1" max="12"
                                        placeholder="{{ __('Frequency Interval Month') }}" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>{{ __('Keyword') }}:</strong>
                                    <input class="form-control" name="keyword_limit" type="number" value="1" step="0.01"
                                        placeholder="{{ __('Keyword Count') }}" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <strong>{{ __('Amount') }}:</strong>
                                    <input class="form-control" name="amount" type="number" value="{{ $plan->amount }}"
                                        min="0" placeholder="{{ __('Amount') }}" />
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="isFree" value="1" id="isFree"
                                        {{ $plan->is_free ? 'checked' : ''}}>
                                    <label class="form-check-label" for="isFree">
                                        Free
                                    </label>
                                </div>
                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 text-right">
                                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection