@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">{{ __('Plans') }}</div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <table class="table table-bordered">
                        <tr>
                            <th>{{ __('No') }}</th>
                            <th>{{ __('Name') }}</th>
                            <th>{{ __('Price') }}</th>
                            <th>{{ __('Keyword') }}</th>
                            <th width="280px">{{ __('Action') }}</th>
                        </tr>
                        @foreach ($plans as $index => $plan)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $plan->name }}</td>
                            <td>{{ $plan->amount }}{{ $plan->currency }} per {{ $plan->frequency_interval }}{{ $plan->frequency }}</td>
                            <td>{{ $plan->keyword_limit }}</td>
                            <td>
                                <form action="{{ route('plans.destroy',$plan->id) }}" method="POST">
                
                                    <a class="btn btn-info" href="{{ route('plans.show', $plan->id) }}">{{ __('Show') }}</a>
                    
                                    {{-- <a class="btn btn-primary" href="{{ route('plans.edit',$plan->id) }}">{{ __('Edit') }}</a>  --}}
                
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-danger">{{ __('Delete') }}</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <div class="card-footer">
                    <div class="col-xs-6">
                        <a class="btn btn-primary" href="{{ route('plans.create') }}">{{ __('Create new Plan') }}</a><br />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
