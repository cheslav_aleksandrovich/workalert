@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Show a Plan') }}</div>

                <div class="card-body">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('Name') }}:</strong>
                            {{ $plan->name }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('Description') }}:</strong>
                            {{ $plan->description }}
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12">
                        <div class="form-group">
                            <strong>{{ __('Cost') }}:</strong>
                            {{ $plan->amount }}{{ $plan->currency }} per {{ $plan->frequency_interval }} {{ $plan->frequency }}
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="col-xs-12">
                        <a class="btn btn-secondary" href="{{ route('plans.index') }}">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
