@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <div class="pull-left">{{ __('Users') }}</div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <table class="table table-bordered text-white">
                        <tr>
                            <th>{{ __('No') }}</th>
                            <th>{{ __('Email') }}</th>
                            <th>{{ __('Stripe Id') }}</th>
                        </tr>
                        @foreach ($users as $user)
                        <tr>
                            <td>{{ $user->id }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->stripe_id }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <div class="card-footer">
                    <div class="col-xs-6">
                        <a class="btn btn-primary"
                            href="{{ route('admin.index.download') }}">{{ __('Download All Emails') }}</a><br />
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3 text-center">
            <div class="card">
                <div class="card-body">
                    <a class="btn btn-primary my-2" href="{{ route('admin.index') }}">{{ __('Show All Users') }}</a><br>
                    <a class="btn btn-primary my-2" href="{{ route('plans.index') }}">{{ __('Show All Plans') }}</a><br>
                    <a class="btn btn-primary my-2"
                        href="{{ route('plans.create') }}">{{ __('Create new Plan') }}</a><br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection