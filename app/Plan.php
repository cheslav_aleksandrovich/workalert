<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'name', 'description', 'paypal_plan_id', 'type',
        'frequency_interval', 'cycles',
        'amount', 'currency', 'keyword_limit', 'is_default',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($plan) {
            // dd($plan->is_default);
            try {
                DB::beginTransaction();
                // Checking if the user is referred by someone
                if ($plan->is_default == 1) {
                    $other_plans = Plan::where([
                        ['id', '!=', $plan->id],
                        ['is_default', '=', 1],
                    ])->get();

                    foreach ($other_plans as $plan) {
                        $plan->is_default = 0;
                        $plan->save();
                    }
                }
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollback();
                throw $th;
            }
        });
    }
}
