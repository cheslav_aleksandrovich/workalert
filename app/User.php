<?php

namespace App;

use App\Keyword;
use Stripe\Coupon;
use Stripe\Stripe;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Subscription;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Notifications\Notifiable;
use Questocat\Referral\Traits\UserReferral;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, Billable, UserReferral;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'referred_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::created(function ($user) {

            // Assigning a user type
            if (!$user->type) {
                $user->type = 0;
                $user->save();
            }

            try {
                DB::beginTransaction();
                // Checking if the user is referred by someone
                if ($user->referred_by) {

                    // Deleting the cookie
                    setcookie('referral', null, -1, '/');

                    // Set Stripe API
                    Stripe::setApiKey(env('STRIPE_SECRET'));

                    // Creating a coupon and customer
                    $user->createUserCoupon(1);
                    $user->createAsStripeCustomer([
                        'coupon' => $user->affiliate_id
                    ]);

                    $referred_user = User::where('affiliate_id', $user->referred_by)->first();

                    if ($referred_user) {

                        // Checking if this is the referreed users first referral
                        if ($referred_user->getAmmountOff() == 1) {
                            // Creating a coupon and customer
                            $referred_user->createUserCoupon($referred_user->getAmmountOff());
                            $referred_user->createAsStripeCustomer([
                                'coupon' => $referred_user->affiliate_id
                            ]);
                        } else {
                            // Since it's not his first referral we have to delete his existing coupon and creating a new one
                            $existing_coupon = Coupon::retrieve($referred_user->affiliate_id);
                            $existing_coupon->delete();
                            $referred_user->createUserCoupon($referred_user->getAmmountOff());
                            // Assiging the user to the new coupon
                            $referred_user->updateStripeCustomer([
                                'coupon' => $referred_user->affiliate_id
                            ]);
                        }
                    }
                }

                // Assigning the user to a free plan on register
                $user->addUserToDefaultPlan();
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollback();
                throw $th;
            }
        });
    }

    public function addUserToDefaultPlan()
    {
        $localPlan = Plan::where('is_default', 1)->get()->last();
        if ($localPlan) {
            $subscription = new \Laravel\Cashier\Subscription;
            $subscription->user_id = $this->id;
            $subscription->name = 'default';
            $subscription->stripe_id = 'fake-id';
            $subscription->stripe_status = 'active';
            $subscription->stripe_plan = $localPlan->stripe_plan_id;
            $subscription->save();
        }
    }

    public function createUserCoupon($amount)
    {
        $coupon = Coupon::create([
            'amount_off' => $amount * 100,
            'currency' => 'usd',
            'duration' => 'forever',
            'name' => '$' . $amount . ' Off',
            'id' => $this->affiliate_id
        ]);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }

    // public function subscriptions()
    // {
    //     return Subscription::find(['user_id' => $this->id]);
    //     // return $this->hasMany(Subscription::class);
    // }

    public function getAmmountOff()
    {
        $referred_by = $this->referred_by ? 1 : 0;
        return $this->where('referred_by', $this->affiliate_id)->count() + $referred_by;
    }

    public function getAmmountOfKeywords()
    {
        $referred_by = $this->referred_by ? 1 : 0;
        return $this->where('referred_by', $this->affiliate_id)->count() + $referred_by + 1;
    }

    public function keywords()
    {
        $cur_plan = $this->current_plan();
        if ($cur_plan) {
            $limit = $cur_plan->keyword_limit;
        } else {
            $limit = 0;
        }
        return Keyword::where('user_id', $this->id)->take($limit)->get();
    }

    public function all_keywords()
    {
        return Keyword::where('user_id', $this->id)->get();
        // return $this->hasMany(Keyword::class, 'user_id');
    }

    public function mobile_keywords()
    {
        return $this->hasMany(MobileSubscription::class, 'user_id');
    }

    public function current_plan()
    {
        $subscription = $this->checkSubscription();
        if ($subscription) {
            return $subscription->getPlan()->first();
        } else {
            return null;
        }
    }

    public function checkSubscription()
    {
        return $this->subscriptions()->active()->get()->first();
    }

    // public function subscription()
    // {
    //     return Subscription::whereDate(
    //         'expired_at',
    //         '>=',
    //         date('Y-m-d') . ' 00:00:00'
    //     )->where('user_id', $this->id)->first();
    // }
}
