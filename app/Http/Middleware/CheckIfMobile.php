<?php

namespace App\Http\Middleware;

use Closure;
use Jenssegers\Agent\Agent;

class CheckIfMobile
{
    public $agent;

    public function __construct()
    {
        $this->agent = new Agent();
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$this->agent->isMobile()) {
            $message = ["message" => "Permission Denied"];
            return response($message, 403);
        }

        return $next($request);
    }
}
