<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class ApiAuthController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'type' => 'integer',
        ]);
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }
        $request['name'] = "user";
        $request['password'] = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $request['type'] = $request['type'] ? $request['type']  : 0;
        $user = User::create($request->toArray());
        // $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        // $response = ['token' => $token];
        $response = $this->getUserResponse($user);
        return response($response, 200);
    }

    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                // $token = $user->createToken('Laravel Password Grant Client')->accessToken;
                // $response['token'] = $token;
                // $response['username'] = $user->name;
                // $response['email'] = $user->email;
                // $response['current_plan'] = $user->current_plan();
                // $response['keywords'] = $user->keywords();
                $response = $this->getUserResponse($user);
                return response($response, 200);
            } else {
                $response = ["message" => "Password mismatch"];
                return response($response, 422);
            }
        } else {
            $response = ["message" => 'User does not exist'];
            return response($response, 422);
        }
    }

    public function user(Request $request)
    {
        $response = $this->getUserResponse($request->user());
        // return $request->user();
        return response($response, 200);
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }

    public function getAuthUser()
    {
        $user = auth()->user();
        // $response['username'] = $user->name;
        // $response['email'] = $user->email;
        // $response['current_plan'] = $user->current_plan();
        // $response['keywords'] = $user->keywords();
        $response = $this->getUserResponse($user);
        return response($response, 200);
    }

    public function forgotPassword(Request $request)
    {
        $credentials = request()->validate(['email' => 'required|email|exists:users']);

        Password::sendResetLink($credentials);

        return response()->json(["msg" => 'Reset password link sent on your email id.']);
    }

    public function getUserResponse(User $user)
    {
        $response['token'] = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response['username'] = $user->name;
        $response['email'] = $user->email;
        $response['current_plan'] = $user->current_plan();
        $response['keywords'] = $user->keywords();
        $response['affiliate_id'] = $user->affiliate_id;
        $response['affiliate_url'] = $user->getReferralLink();
        $response['referred_by'] = $user->referred_by;
        $response['referral_points'] = $user->getAmmountOff();
        return $response;
    }
}
