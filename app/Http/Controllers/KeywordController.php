<?php

namespace App\Http\Controllers;

use App\User;
use App\Keyword;
use App\MobileSubscription;
use Illuminate\Http\Request;

class KeywordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $id = $user->id;

        if (auth()->user()->all_keywords()->count() == auth()->user()->getAmmountOfKeywords() or auth()->user()->all_keywords()->count() > auth()->user()->getAmmountOfKeywords()) {
            return 'You have exceeded your keyword limit';
            die();
        }

        $request->validate([
            'keyword' => 'required',
            'rss' => 'required',
        ]);

        $request['user_id'] = $id;

        Keyword::create($request->toArray());

        if ($user->referred_by) {
            $referred_user = User::where('affiliate_id', $user->referred_by)->get()->first();

            $subscription = MobileSubscription::where(['user_id' => $referred_user->id, 'referrer_id' => $user->id])->get()->first();

            if (!$subscription) {
                MobileSubscription::create([
                    'user_id' => $referred_user->id,
                    'referrer_id' => $user->id
                ]);
            }
        }

        return back()->with('success', 'Keyword added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function show(Keyword $keyword)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function edit(Keyword $keyword)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Keyword $keyword)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Keyword  $keyword
     * @return \Illuminate\Http\Response
     */
    public function destroy(Keyword $keyword)
    {
        $user = auth()->user();
        // $keyword = Keyword::findOrFail($id);
        $keyword->delete();

        return back()->with('success', 'Keyword deleted successfully.');
    }
}
