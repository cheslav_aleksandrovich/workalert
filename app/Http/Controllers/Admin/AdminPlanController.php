<?php

namespace App\Http\Controllers\Admin;

use Stripe\Plan;
use Stripe\Stripe;
// use PayPal\Api\Plan;

use PayPal\Api\Patch;
use PayPal\Api\Payer;
use PayPal\Api\Currency;
use PayPal\Api\Agreement;
use App\Plan as LocalPlan;
use PayPal\Api\ChargeModel;
use PayPal\Rest\ApiContext;
use Illuminate\Http\Request;
use PayPal\Api\PatchRequest;
use PayPal\Common\PayPalModel;

use PayPal\Api\ShippingAddress;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\MerchantPreferences;
use App\Http\Controllers\Controller;
use PayPal\Auth\OAuthTokenCredential;
use Stripe\Product;

class AdminPlanController extends Controller
{
    private $apiContext;
    private $mode;
    private $client_id;
    private $secret;

    public function __construct()
    {
        // Detect if we are running in live mode or sandbox
        // if (config('paypal.settings.mode') == 'live') {
        //     $this->client_id = config('paypal.live_client_id');
        //     $this->secret = config('paypal.live_secret');
        // } else {
        //     $this->client_id = config('paypal.sandbox_client_id');
        //     $this->secret = config('paypal.sandbox_secret');
        // }

        // // Set the Paypal API Context/Credentials
        // $this->apiContext = new ApiContext(new OAuthTokenCredential($this->client_id, $this->secret));
        // $this->apiContext->setConfig(config('paypal.settings'));

        Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = LocalPlan::all();
        return view('admin.plans.index', compact('plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'frequency_interval' => 'required',
            'amount' => 'required',
            'keyword_limit' => 'required',
        ]);

        $request['type'] = 'infinite';
        $request['frequency'] = 'Month';
        $request['cycles'] = '0';
        $request['currency'] = 'USD';
        $request['auto_bill_amount'] = 'yes';
        $request['initial_fail_amount_action'] = 'CONTINUE';
        $request['is_free'] = (bool) $request->isFree;
        $request['is_default'] = (bool) $request->isDefault;
        // $request['paypal_plan_id'] = 'FAKE-PAYPAL-PLAN-ID';

        $localPlan = LocalPlan::create($request->all());
        // exit;

        try {
            $stripe_product = Product::create([
                "name" => $request['name'],
                "description" => $request['description'],
            ]);

            $stripe_plan = Plan::create(array(
                "amount" => $request['amount'] * 100,
                "interval" => "month",
                "product" => $stripe_product['id'],
                "currency" => $request['currency'],
                "interval_count" => $request['frequency_interval'],
            ));
        } catch (\Stripe\Exception\CardException $e) {
            back()->withErrors($e->getError()->message);
        }

        // $localPlan->is_free = (bool) $request->isFree;
        // $localPlan->is_default = (bool) $request->isDefault;
        $localPlan->stripe_prod_id = $stripe_product['id'];
        $localPlan->stripe_plan_id = $stripe_plan['id'];
        $localPlan->save();


        /** Paypal Integration 
         * 
         * 
         * 

        $plan = new Plan();
        $plan->setName($request['name'])
            ->setDescription($request['description'])
            ->setType($request['type']);

        // Set billing plan definitions
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Regular Payments')
            ->setType('REGULAR')
            ->setFrequency($request['frequency'])
            ->setFrequencyInterval($request['frequency_interval'])
            ->setCycles($request['cycles'])
            ->setAmount(new Currency(array(
                'value' => $request['amount'],
                'currency' => $request['currency']
            )));

        // Set merchant preferences
        $merchantPreferences = new MerchantPreferences();
        $merchantPreferences->setReturnUrl(route('paypal.return', $localPlan->id))
            ->setCancelUrl(route('paypal.return', $localPlan->id))
            ->setAutoBillAmount($request['auto_bill_amount'])
            ->setInitialFailAmountAction($request['initial_fail_amount_action'])
            ->setMaxFailAttempts('0');

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        //create the plan
        try {
            $createdPlan = $plan->create($this->apiContext);

            try {
                $patch = new Patch();
                $value = new PayPalModel('{"state":"ACTIVE"}');
                $patch->setOp('replace')
                    ->setPath('/')
                    ->setValue($value);
                $patchRequest = new PatchRequest();
                $patchRequest->addPatch($patch);
                $createdPlan->update($patchRequest, $this->apiContext);
                $plan = Plan::get($createdPlan->getId(), $this->apiContext);

                // Output plan id
                $localPlan->paypal_plan_id = $plan->getId();
                $localPlan->save();
            } catch (PayPal\Exception\PayPalConnectionException $ex) {
                // echo $ex->getCode();
                // echo $ex->getData();
                back()->withErrors($ex->getData());
            } catch (Exception $ex) {
                back()->withErrors($ex->getData());
            }
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            // echo $ex->getCode();
            // echo $ex->getData();
            // die($ex);
            back()->withErrors($ex->getData());
        } catch (Exception $ex) {
            // die($ex);
            back()->withErrors($ex->getData());
        }

         */


        return redirect()->route('plans.index')
            ->with('success', 'Plan created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function show(LocalPlan $plan)
    {
        return view('admin.plans.show', compact('plan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function edit(LocalPlan $plan)
    {
        return view('admin.plans.edit', compact('plan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LocalPlan $plan)
    {
        $request->validate([
            'name' => 'required',
            'detail' => 'required',
        ]);

        $request['is_free'] = $request->isFree;

        $plan->update($request->all());

        return redirect()->route('admin.plans.index')
            ->with('success', 'Plan updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function destroy(LocalPlan $plan)
    {
        if ($plan->is_free) {
            $plan->delete();

            return redirect()->route('plans.index')
                ->with('success', 'Plan deleted successfully');
        }

        try {
            // (new Plan)->delete($plan->stripe_plan_id, []);
            // (new Product)->delete($plan->stripe_prod_id, []);
            $update_product = Product::update($plan->stripe_prod_id, [
                "active" => false
            ]);
            $update_plan = Plan::update($plan->stripe_plan_id, [
                "active" => false
            ]);
            $plan->delete();
            return redirect()->route('plans.index')
                ->with('success', 'Plan deleted successfully');
        } catch (\Exception $e) {
            dd($e);
            return back()->withErrors($e->getMessage());
        }

        // Create a new billing plan
        // $plan = new Plan();
        // $localPlan = LocalPlan::find($planId);
        // $plan->setId($localPlan->paypal_plan_id);

        // //create the plan
        // try {
        //     $response = $plan->delete($this->apiContext);
        //     $localPlan->delete();

        //     return redirect()->route('plans.index')
        //         ->with('success', 'Plan deleted successfully');
        // } catch (PayPal\Exception\PayPalConnectionException $ex) {
        //     return back()->withErrors($ex->getData());
        // } catch (Exception $ex) {
        //     return back()->withErrors($ex->getData());
        // }
    }
}
