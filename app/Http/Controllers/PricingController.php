<?php

namespace App\Http\Controllers;

use App\Plan;
use Illuminate\Http\Request;

class PricingController extends Controller
{
    public function index()
    {
        // dd(auth()->user()->keywords()->count());
        // dd(auth()->user()->getAmmountOff());
        $plans = Plan::all();
        return view('pricing', compact('plans'));
    }
}
