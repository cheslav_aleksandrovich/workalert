<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\controller;
use App\Keyword;
use App\MobileSubscription;
use App\Plan;
use App\User;
use Auth;

class KeywordAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        // $current_plan = $user->current_plan();

        return [
            // 'plans' => Plan::all(),
            // 'current_plan' => $user->current_plan(),
            'keywords' => $user->all_keywords(),
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        $id = Auth::id();
        $request['user_id'] = $id;
        Keyword::create($request->toArray());

        if ($user->referred_by) {
            $referred_user = User::where('affiliate_id', $user->referred_by)->get()->first();

            $subscription = MobileSubscription::where(['user_id' => $referred_user->id, 'referrer_id' => $user->id])->get()->first();

            if (!$subscription) {
                MobileSubscription::create([
                    'user_id' => $referred_user->id,
                    'referrer_id' => $user->id
                ]);
            }
        }

        return [
            'keywords' => $user->all_keywords(),
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Keyword::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $keyword = Keyword::findOrFail($id);
        $keyword->update($request->all());

        return $keyword;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Auth::user();
        $keyword = Keyword::findOrFail($id);
        $keyword->delete();

        return [
            'keywords' => $user->all_keywords(),
        ];
    }
}
