<?php

namespace App\Http\Controllers;

use Auth;

use App\User;
use Stripe\Coupon;

use Stripe\Stripe;
use PayPal\Api\Payer;
use PayPal\Api\Agreement;
use App\Plan as LocalPlan;
use PayPal\Rest\ApiContext;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PayPal\Api\Plan as PaypalPlan;
use Laravel\Cashier\SubscriptionItem;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\AgreementStateDescriptor;
use Laravel\Cashier\Subscription as CashierSubscription;

class SubscriptionController extends Controller
{
    private $apiContext;
    private $mode;
    private $client_id;
    private $secret;

    // Create a new instance with our paypal credentials
    public function __construct()
    {

        // Detect if we are running in live mode or sandbox
        // if (config('paypal.settings.mode') == 'live') {
        //     $this->client_id = config('paypal.live_client_id');
        //     $this->secret = config('paypal.live_secret');
        // } else {
        //     $this->client_id = config('paypal.sandbox_client_id');
        //     $this->secret = config('paypal.sandbox_secret');
        // }

        // // Set the Paypal API Context/Credentials
        // $this->apiContext = new ApiContext(new OAuthTokenCredential($this->client_id, $this->secret));
        // $this->apiContext->setConfig(config('paypal.settings'));

        Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    public function index()
    {
        $subscriptions = Auth::user()->subscriptions();
        return view('subscriptions.index', compact('subscriptions'));
    }

    public function paypalRedirect($planId)
    {
        // Create new agreement
        $agreement = new Agreement();
        $agreement->setName('App Name Monthly Subscription Agreement')
            ->setDescription('Basic Subscription')
            ->setStartDate(\Carbon\Carbon::now()->addMinutes(5)->toIso8601String());

        // Set plan id
        $plan = new PaypalPlan();
        $plan->setId($planId);
        $agreement->setPlan($plan);

        // Add payer type
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $agreement->setPayer($payer);

        try {
            // Create agreement
            $agreement = $agreement->create($this->apiContext);

            // Extract approval URL to redirect user
            $approvalUrl = $agreement->getApprovalLink();

            return redirect($approvalUrl);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            back()->withErrors($ex->getData());
        } catch (Exception $ex) {
            back()->withErrors($ex->getData());
        }
    }

    public function stripeRedirect($planId, Request $request)
    {
        $localPlan = LocalPlan::where('stripe_plan_id', $planId)->get()->last();

        if (!$localPlan) {
            echo "Plan not found.";
        }

        if ($localPlan->is_free) {
            $subscription = new CashierSubscription;
            $subscription->user_id = auth()->id();
            $subscription->name = 'default';
            $subscription->stripe_id = 'fake-id';
            $subscription->stripe_status = 'active';
            $subscription->stripe_plan = $localPlan->stripe_plan_id;
            $subscription->save();
            return redirect()->away('https://mind2matter.co/thank-you-7');
        }

        return view('subscriptions.stripe', [
            'intent' => auth()->user()->createSetupIntent(),
            'planId' => $planId,
            'try_out_trial' => $request->try_out_trial == 'true' ? true : false,
            'localPlan' => $localPlan
        ]);
    }

    public function paypalReturn($localPlanId, Request $request)
    {
        $localPlan = LocalPlan::find($localPlanId);

        $token = $request->token;
        $agreement = new Agreement();

        try {
            // Execute agreement
            $result = $agreement->execute($token, $this->apiContext);
            $user = Auth::user();
            if (isset($result->id)) {
                $subscription = Subscription::create([
                    'user_id' => $user->id,
                    'plan_id' => $localPlanId,
                    'paypal_agreement_id' => $result->id,
                    'currency' => $localPlan->currency,
                    'amount' => $localPlan->amount,
                    'expired_at' => date('Y-m-d H:i:s', strtotime($result->agreement_details->next_billing_date)),
                ]);
                $user->paypal_agreement_id = $result->id;
            } else {
                return back()->withErrors('Can not find agreement id.');
            }

            // return redirect()->route('pricing')
            //     ->with('success', 'New Subscriber Created and Billed.');

            return redirect()->to('https://mind2matter.co/thank-you-7/');
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            return back()->withErrors(
                'You have either cancelled the request or your session has expired'
            );
        }
    }

    public function stripeSubscribe(Request $request)
    {
        $planID = $request->planID;
        $paymentMethod = $request->paymentMethod;
        $referrCode = $request->referrCode;
        $user = auth()->user();
        $checkSubscription = $user->checkSubscription();
        $localPlan = LocalPlan::where('stripe_plan_id', $planID)->get()->last();

        if (!$localPlan) {
            return response()->json(['errors' => 'Plan not found.'], 422);
        }

        if ($referrCode) {

            try {
                DB::beginTransaction();
                if ($user->referred_by) {
                    return response()->json(['errors' => 'You already have a referrer.'], 422);
                }

                if ($user->affiliate_id == $referrCode) {
                    return response()->json(['errors' => 'You can\'t use your own referr code.'], 422);
                }

                $referred_user = User::where('affiliate_id', $referrCode)->first();

                if (!$referred_user) {
                    return response()->json(['errors' => 'Code Not Found.'], 422);
                }

                $user->referred_by = $referrCode;
                $user->save();

                // Creating a coupon and customer
                $user->createUserCoupon(1);

                if ($user->stripe_id) {
                    $user->updateStripeCustomer([
                        'coupon' => $user->affiliate_id
                    ]);
                } else {
                    $user->createAsStripeCustomer([
                        'coupon' => $user->affiliate_id
                    ]);
                }

                // Checking if this is the referreed users first referral
                if ($referred_user->getAmmountOff() == 1) {
                    // Creating a coupon and customer
                    $referred_user->createUserCoupon($referred_user->getAmmountOff());
                    $referred_user->createAsStripeCustomer([
                        'coupon' => $referred_user->affiliate_id
                    ]);
                } else {
                    // Since it's not his first referral we have to delete his existing coupon and creating a new one
                    $existing_coupon = Coupon::retrieve($referred_user->affiliate_id);
                    $existing_coupon->delete();
                    $referred_user->createUserCoupon($referred_user->getAmmountOff());
                    // Assiging the user to the new coupon
                    $referred_user->updateStripeCustomer([
                        'coupon' => $referred_user->affiliate_id
                    ]);
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollBack();
                return response()->json(['errors' => $e->getMessage()], 422);
            }
        }

        try {
            if ($checkSubscription) {
                if ($checkSubscription->stripe_id == 'fake-id') {
                    $sub = CashierSubscription::where('stripe_plan', $checkSubscription->stripe_plan)->get()->first();
                    $sub->delete();
                    $response = $user->newSubscription('default', $planID)->create($paymentMethod, [
                        'email' => $user->email
                    ]);
                } else {
                    $user->subscription('default')->swap($planID);
                }
            } else {
                $response = $user->newSubscription('default', $planID)->create($paymentMethod, [
                    'email' => $user->email
                ]);
            }
            return response()->json(['success' => 'Subcription addedd successfully'], 200);
        } catch (\Exception $e) {
            return response()->json(['errors' => $e->getMessage(), $e->getTrace()], 422);
        }
    }

    public function stripeTrial(Request $request)
    {
        $planID = $request->planID;
        $paymentMethod = $request->paymentMethod;
        $user = auth()->user();
        // $localPlan = LocalPlan::find($planID);
        $checkSubscription = $user->checkSubscription();

        try {
            if ($checkSubscription) {
                $user->subscription('default')->swap($planID);
            } else {
                $response = $user->newSubscription('default', $planID)->trialDays(14)->create($paymentMethod, [
                    'email' => $user->email
                ]);
            }
            return response()->json(['success' => 'Subcription addedd successfully', 200]);
        } catch (\Exception $e) {
            response()->json(['errors' => $e->getMessage()], 422);
        }
    }

    public function paypalCancel($getAgreementId, Request $request)
    {

        $agreementId = $getAgreementId;
        $agreement = new Agreement();
        $agreement->setId($agreementId);

        $agreementStateDescriptor = new AgreementStateDescriptor();
        $agreementStateDescriptor->setNote("Cancel the agreement");

        try {

            // $agreement = Agreement::get($agreement->getId(), $this->apiContext);
            // $details = $agreement->getAgreementDetails();
            // $payer = $agreement->getPayer();
            // $payerInfo = $payer->getPayerInfo();
            // $plan = $agreement->getPlan();
            // $payment = $plan->getPaymentDefinitions()[0];

            $agreement->cancel($agreementStateDescriptor, $this->apiContext);

            return redirect()->route('pricing')
                ->with('success', 'Cancel Subscription has been successfully!');
        } catch (Exception $ex) {
            echo "Failed to get activate";
            var_dump($ex);
            exit();
        }
    }

    public function stripeCancel($planId)
    {
        $localPlan = LocalPlan::where('stripe_plan_id', $planId)->get()->last();

        if (!$localPlan) {
            echo "Plan not found.";
        }

        if ($localPlan->is_free) {
            $sub = CashierSubscription::where('stripe_plan', $planId)->get()->first();
            $sub->delete();

            return redirect()->route('pricing')
                ->with('success', 'Cancel Subscription has been successfully!');
        }

        try {
            auth()->user()->subscription('default', $planId)->cancelNow();

            return redirect()->route('pricing')
                ->with('success', 'Cancel Subscription has been successfully!');
        } catch (\Exception $ex) {
            echo "Failed to get activate";
            var_dump($ex);
            exit();
        }
    }
}
