<?php

namespace App\Http\Controllers\Mobile;

use App\User;
use App\MobileSubscription;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }
        $user = User::where('email', $request->email)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $response = $this->getUserResponse($user);
                return response($response, 200);
            } else {
                $response = ["message" => "Password mismatch"];
                return response($response, 422);
            }
        } else {
            $response = ["message" => 'User does not exist'];
            return response($response, 422);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'type' => 'integer',
        ]);
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }
        $request['name'] = "user";
        $request['password'] = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $request['type'] = 0;
        DB::beginTransaction();
        $user = User::create($request->toArray());

        // $subscription = MobileSubscription::create([
        //     'user_id' => auth()->id(),
        //     'keyword_limit' => 1,
        // ]);

        if ($request->ref) {
            if (!User::where('affiliate_id', $request->ref)) {
                DB::rollBack();
                $response = ["message" => "Affiliate ID not found"];
                return response($response, 422);
            }
            $user->referred_by = $request->ref;
            $user->save();
        }

        DB::commit();
        $response = $this->getUserResponse($user);
        return response($response, 200);
    }

    public function getAuthUser()
    {
        $user = auth()->user();
        $response = $this->getUserResponse($user);
        return response($response, 200);
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }

    public function getUserResponse(User $user)
    {
        $response['token'] = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response['username'] = $user->name;
        $response['email'] = $user->email;
        $response['keywords'] = $user->mobile_keywords()->count();
        // $response['current_plan'] = 'Unlimited Keyword Plan';
        // $response['current_plan'] = ['mobile_plans' => 'Unlimited Keyword Plan', 'web_plans' => $user->current_plan()];
        // $response['keywords'] = ['mobile_keywords' => 'Unlimited Keywords', 'web_keywords' => $user->keywords()];
        // $response['affiliate_url'] = $user->getReferralLink();
        $response['affiliate_id'] = $user->affiliate_id;
        $response['affiliate_url'] = url('api/mobile/register?ref=' . $user->affiliate_id);
        $response['referred_by'] = $user->referred_by;
        // $response['referral_points'] = $user->getAmmountOff();
        return $response;
    }
}
