<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('baseUrl');
    }

    public function baseUrl(Request $request)
    {
        // dd(User::where('affiliate_id', 'B1JWS')->first()->getAmmountOff());
        if (!auth()->check()) {
            if ($request->query('ref')) {
                return redirect()->route('register');
            }
            return redirect()->route('login');
        }
        return redirect()->route('pricing');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
}
